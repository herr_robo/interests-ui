import React from "react";
import { Button, Form, FormControl, InputGroup } from "react-bootstrap";
import AnimeList from "./anime-list";
import AnimeMalList from "./anime-mal-list";

class AnimeSection extends React.Component<{}, { animeList: any[], malList: any[], searchDisabled: boolean }> {

    constructor(props : any) {
        super(props);
        this.getLocalList = this.getLocalList.bind(this);
        this.handleAddToWatchList = this.handleAddToWatchList.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleUpdateList = this.handleUpdateList.bind(this);

        this.state = {
            animeList : [],
            malList : [],
            searchDisabled : true
        };
    }

    componentDidMount() {
        this.getLocalList();
    }

    getLocalList() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Origin', 'http://localhost:3000');
        
        fetch('http://localhost:8080/api/v1/anime', {
            mode: 'cors',
            method: 'GET',
            headers: headers
        })
        .then(response => response.json())
        .then(data => {
            console.log('data', data);
            this.setState({animeList : data});
        })
    }

    handleAddToWatchList(event : any) {
        let list = this.state.animeList;
        list.push(event);

        this.setState({animeList: list});
    }

    handleUpdateList(event : any) {
        this.removeFromStateList(event);
    }

    removeFromStateList(id : number) {
        let list = this.state.animeList;
        let newList = list.filter(item => item.anime_id !== id);
        this.setState({animeList: newList});
    }

    handleSubmit(event : any) {
        event.preventDefault();
        let value = event.target[0].value

        if (value) {
        fetch('https://api.jikan.moe/v3/search/anime?q=' + value)
            .then(response => response.json())
            .then(data => {
                if (data.status === 404) {
                    return;
                }
                this.setState({malList: data.results});
        });
        }
    }

    handleChange(event: any) {
        if (event.target.value.length > 0) {
            this.setState({searchDisabled: false});
        }
        else {
            this.setState({searchDisabled: true});
        }
    }

    render() {
        return (
            <div>
                <div>AnimeSection</div>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            { this.state.animeList.length > 0 
                                ? <AnimeList animeList={this.state.animeList} onChange={this.handleUpdateList} updateEditList={this.getLocalList}/>
                                : ""
                            }
                        </div>
                        <div className="col">
                            <Form onSubmit={this.handleSubmit}>
                                <InputGroup className="mb-3" >
                                    <FormControl
                                        type="text"
                                        placeholder="Search Anime"
                                        aria-label="Search Anime"
                                        onChange={this.handleChange}
                                    />
                                    <InputGroup.Append>
                                        <Button variant="outline-secondary" disabled={this.state.searchDisabled} type="submit" value="Submit">Search</Button>
                                    </InputGroup.Append>
                                </InputGroup>
                            </Form>
                            { this.state.malList.length > 0 
                                ? <AnimeMalList animeMalList={this.state.malList} updateAddList={this.handleAddToWatchList}/>
                                : ""
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AnimeSection;