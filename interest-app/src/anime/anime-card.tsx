
import { useState } from 'react';
import { Accordion, Button, Card, Col, Form, Modal } from 'react-bootstrap';

function AnimeCard(props : any) {
    let anime = props.anime;
    let eventKey = props.animeIndex.toString();

    const [show, setShow] = useState(false);
    const [watchStatus, setWatchStatus] = useState(0);
    const [episodesWatched, setEpisodesWatched] = useState(0);
    const [modalButtonDisabled, setModalButtonDisabled] = useState(false);

    function removeFromList() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Origin', 'http://localhost:3000');

        fetch('http://localhost:8080/api/v1/anime/' + anime.anime_id, {
            mode: 'cors',
            method: 'DELETE',
            headers: headers
        })
        .then(
            props.onChange(anime.anime_id)
        )
    }

    function selectChange(event: any) {
        setWatchStatus(event.target.value);
    }

    function episodeNumberChange(event: any) {
        if ((event.target.value < 0) || (event.target.value > anime.episodes) || (event.target.value === '')) {
            setModalButtonDisabled(true);
        }
        else {
            setModalButtonDisabled(false);
            setEpisodesWatched(event.target.value);
        }
    }

    const handleClose = () => setShow(false);
    const handleShow = () => {
        setModalButtonDisabled(false);
        setShow(true);
    };

    const handleSaveClose = (event: any) => {
        anime.watch_status = watchStatus;
        anime.episodes_watched = episodesWatched;

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Origin', 'http://localhost:3000');

        fetch('http://localhost:8080/api/v1/anime/' + anime.anime_id, {
            mode: 'cors',
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(anime)
        })
        .then(response => response.json())
        .then(data => {
            console.log('Success: ', data);
            props.updateEdit();
        })
        .catch((error) => {
            console.error('Error: ', error);
        });

        handleClose();
    }

    function determineDate(input : any) {
        let date = new Date(input);
        let dateString = date.toLocaleDateString('en-US');
        return dateString;
    }

    return (
        <Card style={{ width: '20rem', height: '100%'}}>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey={eventKey}>
                    <Card.Title style={{textAlign: 'left'}}>{anime.title}</Card.Title>
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={eventKey}>
                <Card.Body style={{color: '#282c34'}}>
                    <Card.Img variant="top" src={anime.image_url}/>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Card.Text>
                            {anime.anime_type} - {anime.rated}
                        </Card.Text>
                        <div className="remove-from-local">
                            <Button onClick={removeFromList} variant="outline-primary">Remove from List</Button>
                        </div>
                    </div>
                    <Card.Text>Total Episodes: {anime.episodes}</Card.Text>
                    <Card.Text>{anime.airing ? 'Show currently airing' : 'Show has ended'}</Card.Text>
                    <Card.Text>{determineDate(anime.start_date)} to {determineDate(anime.end_date)}</Card.Text>
                    <Card.Text>Episodes Seen: {anime.episodes_watched} / {anime.episodes}</Card.Text>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Card.Text>
                            Watch Status:
                            {(() => {
                                switch (anime.watch_status) {
                                case 0:  return " No Plans";
                                case 1:  return " Plan to Watch";
                                case 2:  return " Watching";
                                case 3:  return " On Hold";
                                case 4:  return " Completed";
                                case 9:  return " Dropped";
                                default:   return " No Plans";
                                }
                            })()}
                        </Card.Text>
                        <div className="update-from-local">
                            <Button onClick={handleShow} variant="outline-primary">Update</Button>
                        </div>
                    </div>
                    <Card.Link href={anime.url}>MAL Page</Card.Link>
                </Card.Body>
            </Accordion.Collapse>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Watch Progress</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="animeForm.SelectWatchStatus">
                            <Form.Label>Watch Status</Form.Label>
                            <Form.Control as="select" custom onChange={selectChange}>
                                <option value="0">No Plans</option>
                                <option value="1">Plan to Watch</option>
                                <option value="2">Watching</option>
                                <option value="3">On Hold</option>
                                <option value="4">Completed</option>
                                <option value="9">Dropped</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="animeForm.Input">
                            <Form.Label>Episodes Watched:</Form.Label>
                            <Form.Row>
                                <Col xs={3}>
                                    <Form.Control type="number" min="0" max={anime.episodes} defaultValue={anime.episodes_watched} onChange={episodeNumberChange}/>
                                </Col>
                                <Col>
                                    <div style={{padding: '6px 12px 6px 12px'}}>
                                        / {anime.episodes}
                                    </div>
                                </Col>
                            </Form.Row>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" disabled={modalButtonDisabled} onClick={handleSaveClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </Card>
    )
}

export default AnimeCard;