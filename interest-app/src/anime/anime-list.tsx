import { Accordion } from 'react-bootstrap';

import AnimeCard from './anime-card';

function AnimeList(props : any) {
    const list = props.animeList;

    function handleChange(event: any) {
        props.onChange(event);
    }

    function handleEdit() {
        props.updateEditList();
    }

    return(
        <div style={{ display: 'flex', flexWrap: 'wrap'}}>
            <Accordion>
                {list.map((value : any, index : any) => {
                    return(
                        <div key={index} style={{ fontSize: '14px', textAlign: 'left'}}>
                            <AnimeCard anime={value} animeIndex={index} onChange={handleChange} updateEdit={handleEdit} />
                        </div>
                    )
                })}
            </Accordion>
        </div>
    )
}

export default AnimeList;