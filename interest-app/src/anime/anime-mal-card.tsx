
import { useState } from 'react';
import { Accordion, Button, Card } from 'react-bootstrap';

function AnimeMalCard(props : any) {
    let anime = props.anime;
    let eventKey = props.animeIndex.toString();

    const [disableAdd, setDisableAdd] = useState(false);

    function addToLocalList() {
        setDisableAdd(true);

        anime.watch_status = 1;
        anime.anime_type = anime.type;

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        headers.append('Origin', 'http://localhost:3000');

        fetch('http://localhost:8080/api/v1/anime', {
            mode: 'cors',
            method: 'POST',
            headers: headers,
            body: JSON.stringify(anime)
        })
        .then(response => response.json())
        .then(data => {
            props.animeAdded(data);
            setDisableAdd(true);
        })
        .catch((error) => {
            console.error('Error: ', error);
        });
    }

    function determineDate(input : any) {
        let date = new Date(input);
        let dateString = date.toLocaleDateString('en-US');
        return dateString;
    }

    return (
        <Card style={{ width: '20rem', height: '100%'}}>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey={eventKey}>
                    <Card.Title style={{textAlign: 'left'}}>{anime.title}</Card.Title>
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey={eventKey}>
                <Card.Body style={{color: '#282c34'}}>
                    <Card.Img variant="top" src={anime.image_url}/>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Card.Text>
                            {anime.type} - {anime.rated}
                        </Card.Text>
                        <div className="add-to-local">
                            <Button onClick={addToLocalList} disabled={disableAdd} variant="outline-primary">Add to List</Button>
                        </div>
                    </div>
                    <Card.Text>Total Episodes: {anime.episodes}</Card.Text>
                    <Card.Text>{anime.airing ? 'Show currently airing' : 'Show has ended'}</Card.Text>
                    <Card.Text>{determineDate(anime.start_date)} to {determineDate(anime.end_date)}</Card.Text>
                    <Card.Link href={anime.url}>MAL Page</Card.Link>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
}

export default AnimeMalCard;