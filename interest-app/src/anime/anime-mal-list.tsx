import { Accordion } from 'react-bootstrap';

import AnimeMalCard from './anime-mal-card';

function AnimeMalList(props : any) {
    const list = props.animeMalList;

    function updateAddAnime(event: any) {
        props.updateAddList(event);
    }

    return(
        <div style={{ display: 'flex', flexWrap: 'wrap'}}>
            <Accordion>
                {list.map((value : any, index : any) => {
                    return(
                        <div key={index} style={{ fontSize: '14px', textAlign: 'left'}}>
                            <AnimeMalCard anime={value} animeIndex={index} animeAdded={updateAddAnime}/>
                        </div>
                    )
                })}
            </Accordion>
        </div>
    )
}

export default AnimeMalList;