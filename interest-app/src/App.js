import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AnimeSection from './anime/anime-section';
import WelcomeSection from './welcome/welcome-section';

function App() {
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <WelcomeSection />
          <AnimeSection />
        </header>
      </div>
  );
}

export default App;
