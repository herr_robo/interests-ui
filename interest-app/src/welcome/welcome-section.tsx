function WelcomeSection() {

    return (
        <div style={{ fontSize: '16px'}}>
            <p>
                Welcome to the Anime Interest Page. You can submit a search on anime titles, 
                and then save which ones that you would like to watch to a list. The idea is to select 
                and save anime shows and movies that you would like to watch to a list, and later 
                update your list with your progress.
            </p>
        </div>
    );

}

export default WelcomeSection;